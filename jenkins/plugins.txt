https://plugins.jenkins.io/dark-theme/
https://plugins.jenkins.io/matrix-auth/
https://plugins.jenkins.io/ssh-slaves/
https://plugins.jenkins.io/git/
https://plugins.jenkins.io/workflow-aggregator/
https://plugins.jenkins.io/ws-cleanup/
https://plugins.jenkins.io/timestamper/
https://plugins.jenkins.io/credentials-binding/
https://plugins.jenkins.io/build-timeout/
https://plugins.jenkins.io/dashboard-view/
https://plugins.jenkins.io/gitlab-plugin/
https://plugins.jenkins.io/envinject/
https://plugins.jenkins.io/pipeline-stage-view/

