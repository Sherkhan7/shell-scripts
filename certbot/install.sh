#!/bin/bash

# Install snapd
sudo apt-get install snapd -y

# Update the snapd package to ensure that the latest version is installed on the machine.
sudo snap install core; sudo snap refresh core

# Remove the Certbot package if it is already installed on the machine.
sudo apt remove certbot

# Run this command on the command line on the machine to install Certbot.
sudo snap install --classic certbot

# Execute the following instruction on the command line on the machine to ensure that the certbot command can be run.
sudo ln -s /snap/bin/certbot /usr/bin/certbot
